Este template de aplicação Rails tem como objetivo agilizar o bootstrap de novas APIs da Vnda. Ele pode ser utilizado também em aplicações existentes, mas é necessário verificar as modificações feitas pelo template para que nada fique quebrado.

### Rails

Certifique-se que você possui a última versão do Rails ao gerar a aplicação.

### Gems

Algumas das gems mais utilizadas nos projetos da Vnda são incluídas automaticamente por este template.

### Database

O `database.yml` é gerado automaticamente baseado em informações que são perguntadas no momento da geração da app. Antes de criar a aplicação, sugerimos que você solicite uma cópia do banco de dados, pois no momento da geração dos models, uma base de dados será necessária.

### RSpec

Ele estará pronto para ser utilizado e com as configurações utilizadas nos projetos da Vnda.

### Deployment

O deployment é feito utilizando Mina, então é preciso ajustar o arquivo `config/deploy.rb` com os dados do servidor onde a aplicação vai ser hospedada.

Uso o comando abaixo para gerar a aplicação rails:

    rails new API_NAME --template https://bitbucket.org/vnda/api_template/raw/master/app.rb --skip-spring --database=postgresql --skip-test-unit --no-rc
