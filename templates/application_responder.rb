class ApplicationResponder
  delegate :respond, to: :responder

  def initialize(controller, resources, options)
    @controller = controller
    @resource = resources.pop
    @resources = resources
    @options = options
  end

  def self.call(*args)
    new(*args).respond
  end

  protected

  def responder
    if @resource.respond_to?(:page)
      limit = @controller.params[:limit].presence || 100
      @resource = @resource.page(@controller.params[:page]).per([limit.to_i, 100].min)

      @controller.headers['X-Pagination'] = {
        total_pages: @resource.total_pages,
        current_page: @resource.current_page,
        prev_page: !@resource.first_page?,
        next_page: !@resource.last_page?
      }.to_json
    end

    ActionController::Responder.new(@controller, @resources.push(@resource), @options)
  end
end
