require 'mina/bundler'
require 'mina/rails'
require 'mina/git'

set :repository, '...'
set :branch, 'master'
set :shared_paths, ['config/database.yml', 'config/newrelic.yml', 'config/secrets.yml', 'log', 'tmp/pids', 'tmp/sockets']
set :ssh_options, '-A'
set :keep_releases, 3
set :deploy_to, proc { "/home/#{user}/..." }

set :bundle_options, proc { "--without development:test:deploy:#{bundle_without} --path \"#{bundle_path}\" --deployment" }

task :environment do
end

task :staging => :environment do
  set :domain, ''
  set :user, ''
  set :bundle_without, 'production'
  set :rails_env, 'staging'
end

task :production => :environment do
  set :domain, ''
  set :user, ''
  set :bundle_without, 'staging'
  set :rails_env, 'production'
end

task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]

  queue! %[touch "#{deploy_to}/shared/config/database.yml"]
  queue!  %[echo "-----> Be sure to edit 'shared/config/database.yml'."]

  queue! %[touch "#{deploy_to}/shared/config/newrelic.yml"]
  queue!  %[echo "-----> Be sure to edit 'shared/config/newrelic.yml'."]

  queue! %[touch "#{deploy_to}/shared/config/secrets.yml"]
  queue!  %[echo "-----> Be sure to edit 'shared/config/secrets.yml'."]

  queue! %[mkdir -p "#{deploy_to}/shared/tmp/pids"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/tmp/pids"]

  queue! %[mkdir -p "#{deploy_to}/shared/tmp/sockets"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/tmp/sockets"]

  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/tmp"]
end

task :start do
  queue! "sudo start ..."
end

task :stop do
  queue! "sudo stop ..."
end

task :restart do
  queue! "sudo restart ..."
end

namespace :newrelic do
  task :notify do
    if rails_env == "production"
      in_directory "#{deploy_to}/current" do
        queue! "bundle exec newrelic deployments -e #{rails_env}"
      end
    end
  end
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'

    to :launch do
      invoke :restart
      invoke :"newrelic:notify"
    end
  end
end
