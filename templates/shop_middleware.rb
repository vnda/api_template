class ShopMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    env["current_shop"] = find_shop(env)
    ::Apartment::Tenant.switch(env["current_shop"].subdomain)

    auth = Rack::Auth::Basic.new(@app, "Vnda") do |username, password|
      env["current_shop"].authenticate(username, password)
    end

    auth.call(env)
  rescue ActiveRecord::RecordNotFound
    [404, { "Content-Type" => "application/json" }, [{ error: { message: "Not Found" } }.to_json]]
  end

  protected

  def find_shop(env)
    request = Rack::Request.new(env)
    Rails.cache.fetch(request.host, namespace: "shop", expires_in: 1.minute) do
      ::Shop.find_by!(host: request.host)
    end
  end
end
