Apartment.configure do |config|
  config.use_schemas = true
  config.persistent_schemas = %w[common extensions]
  config.tenant_names = proc do
    sql = "SELECT nspname
      FROM pg_namespace
      WHERE has_schema_privilege(nspname, 'USAGE')
      AND nspname NOT IN('information_schema', 'public', 'common', 'extensions')
      AND nspname !~ 'pg_'
      ORDER BY nspname"
    ActiveRecord::Base.connection.select_values(sql)
  end
end
