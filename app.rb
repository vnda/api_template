# CLEANUP
remove_file "app/assets/"
remove_file "app/controllers/concerns/"
remove_file "app/helpers/"
remove_file "app/mailers/"
remove_file "app/models/concerns/"
remove_file "app/views/"
remove_file "config/initializers/assets.rb"
remove_file "lib/assets/"
remove_file "public/favicon.ico"
remove_file "README.rdoc"
remove_file "test/"
remove_file "vendor/assets/"

# GEMS
gsub_file "Gemfile", /^\s*#.*\n/, ""
gsub_file "Gemfile", /gem\s.*\n/, ""
gsub_file "Gemfile", /^\s*\n$/, ""

gem "oj_mimic_json", "1.0.1"
gem "rails", Rails.version
gem "kaminari", "0.16.1"
gem "pg", "0.17.1"
gem "apartment", "0.25.1"
gem "active_model_serializers", "0.8.1"
gem "puma", "2.9.0"
gem "dalli", "2.7.2"
gem "oj", "2.10.2", require: false

gem_group :development do
  gem "rspec-rails", "3.0.2"
  gem "mina", "0.3.1", require: false
end

# CONFIGURATION
gsub_file "app/controllers/application_controller.rb", /^\s*#.*\n/, ""
gsub_file "app/controllers/application_controller.rb", /(protect_from_forgery with:).+/, "\\1 :null_session"
inject_into_file "app/controllers/application_controller.rb", "\n  self.responder = ApplicationResponder\n  respond_to :json", after: /null_session/
append_file ".gitignore", "config/database.yml\nconfig/secrets.yml\n"
gsub_file ".gitignore", "/tmp\n", ""
append_file ".gitignore", "tmp/cache\n"
comment_lines "config/application.rb", /action_(mailer|view)\/railtie/
comment_lines "config/environments/development.rb", /action_mailer/
comment_lines "config/environments/test.rb", /action_mailer/
gsub_file "config/application.rb", /.*sprockets\/railtie.*\n/, ""
file "tmp/pids/.keep"
file "tmp/sockets/.keep"

application do
  g = {
    assets: false,
    helper: false,
    template_engine: false,
    view: false,
    orm: ":active_record, migration: false",
    test_framework: ":rspec, fixture: true" }.map do |k, v|
    "g.#{k} #{v}"
  end
  "config.generators do |g|\n      #{g.join("\n      ")}\n    end\n"
end

application do
  "config.middleware.use \"ShopMiddleware\"\n"
end

# ROUTES
gsub_file "config/routes.rb", /^\s*#.*\n/, ""
route "scope path: \"/api\", format: \"json\" do\n    # put routes here\n  end"

# DEPLOY
get "#{File.dirname(__FILE__)}/templates/deploy.rb", "config/deploy.rb"

# FILES
get "#{File.dirname(__FILE__)}/templates/shop_middleware.rb", "app/middlewares/shop_middleware.rb"
get "#{File.dirname(__FILE__)}/templates/application_responder.rb", "app/responders/application_responder.rb"
get "#{File.dirname(__FILE__)}/templates/staging.rb", "config/environments/staging.rb"
get "#{File.dirname(__FILE__)}/templates/apartment.rb", "config/initializers/apartment.rb"
get "#{File.dirname(__FILE__)}/templates/serializer.rb", "config/initializers/serializer.rb"

database = ask("Enter the database name for development: [site_development]").presence || "site_development"

dev_config = {
  "adapter" => "postgresql",
  "encoding" => "unicode",
  "database" => database,
  "username" => ask("Enter user name for the database #{database}: [#{ENV["USER"]}]").presence || ENV["USER"],
  "password" => ask("Enter password for the database #{database}: ").presence,
  "host" => ask("Enter the database host: [localhost]").presence || "localhost",
  "pool" => 5,
  "schema_search_path" => "public, common, extensions"
}

test_config = dev_config.merge(
  "database" => ask("Enter the database name for test: [site_test]").presence || "site_test"
)

require "yaml"
file "config/database.yml", YAML.dump("development" => dev_config, "test" => test_config), force: true

generate "rspec:install"
generate "model", "shop"

inject_into_class "app/models/shop.rb", "Shop" do
  "  def authenticate(username, password)
    api_username == username && api_password == password
  end\n"
end

gsub_file "spec/models/shop_spec.rb", /^\s*pending.*\n/, ""
inject_into_file "spec/models/shop_spec.rb", before: /^\s*end\s*$/ do
  <<-SPEC
  describe "#authenticate" do
    before do
      subject.api_username = "foo"
      subject.api_password = "bar"
    end

    it "returns true if username and password are valid" do
      expect(subject.authenticate("foo", "bar")).to eq(true)
    end

    it "returns false if username is invalid" do
      expect(subject.authenticate("FOO", "bar")).to eq(false)
    end

    it "returns false if password is invalid" do
      expect(subject.authenticate("foo", "BAR")).to eq(false)
    end
  end
  SPEC
end

git :init
git add: ".", commit: "-m 'first commit'"
